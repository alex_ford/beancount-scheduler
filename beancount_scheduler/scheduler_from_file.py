#!/usr/bin/env python3

"""
This python script focuses on updating a beancount data file with
scheduled transactions. The list of scheduled transactions is kept
in the file `scheduled-transactions.bcspec` which is assumed to reside
in ..\\beancount-data\\scheduled-transactions.bcspec by default.

See notes in the scheduled-transactions.bcspec file for details on
how to setup the transactions themselves.

Current Usage

1. Ensure that scheduled-transactions.beancount contains accurate schedule data
2. Update the startDate and endDate variables in this script
3. Execute this script: python .\\update-scheduled-transactions.py
4. Copy and paste the output into ledger.beancount
5. Validate no errors with beancount interpreter/Fava
6. Manually add links to linked transactions (they should be next to each other)

Author: Alex Richard Ford (arf4188@gmail.com)
"""

###############################################################################
# FEATURES/TODO LIST
###############################################################################

# [x] Uses my Reverse-Engineered Object-Oriented (REOO) Beancount Library code
#       I could have tried to make use of the Beancount code/libraries provided by
#       the main project itself (and I may still want to) but it was a fun learning
#       exercise for me to reverse-engineer them. Improving my Python abilities!

# [x] Reads in transactions from scheduled transactions file

# [x] Computes scheduled transactions for requested date range
#       This is done by taking the initial date provided by the transaction, and applying
#       the 'schedule' metadata value (a timedelta representation) to it, until all the
#       transactions up to the endDate have been computed. Then, it simply discards anything
#       prior to the startDate.

# [x] Auto computes links between linked transactions
#       Links are useful in Beancount when two or more transactions are somehow linked together.
#       This feature automatically creates a new (appropriate) link for transactions marked
#       as being linked in the scheduled transactions file.
#       Links are indicated by using the following syntax: ^{1}
#       It doesn't matter what you put inside the curly braces, but what does matter is that
#       you end up using it repeatedly where you want the same link to appear.
#       What happens: the scheduler replaces your temporary tag "{1}" with a unique short UUID.
#       {1} will be replaced with the same value during the runtime of the scheduler. That is,
#       if you run the scheduler again, {1} will be given a different value.

# [o] Saves/inserts transactions into BC file using --save argument
#       This ignores any existing transactions and simply appends to the bottom of the BC file.
#       If no file param is given, the default ..\beancount-data\ledger.beancount is used.
#       A different file can be specified by doing: --save ..\somewhere\data.beancount
#       See the next feature for an improvement on this.

# [] Option to automatically call the git-sync script in the data directory.
#       This is disabled by default, but can be enabled using the --gitsync option.
#       This expects a git-sync.ps1 script to exist in the same directory as the data file, as
#       specified by the --save argument.

# [] Updates existing transactions for those already in BC file using --update argument
#       If a transaction already exists, then it should be updated!
#       Transactions exist if they match the gen-hash, which is a SHA256 hash generated
#       by my scripts at genesis time of the transaction. 'gen-hash' should NEVER change, even
#       if other attributes of the transaction change. 'gen-hash' can therefore only be
#       created at 'genesis time' of the transaction, and not necessarily later.

# [] Automatically handles credit card statement sorting into statement subaccounts
#       For some credit cards, I like to use statement subaccounts to help track the
#       transactions a bit more accurately to how the credit card company does it.
#       This script can automate the creation of the new subaccounts, when needed,
#       along with outputting the correct subaccount in the transaction entry.

# [] Produces chronologically ordered output
#       The computed transactions from the schedule are output in chronological order based
#       on the configuration of the scheduler.

# [] Configuration can be stored in scheduler.config to change defaults based on user preferences
#       Pretty much everything can be controlled via command line options, but this can be a bit
#       annoying to type out if you need to change the out-of-box defaults. A config file can be
#       used to change the defaults so you don't need to specify the command line options.

# [] Allow for replacement of date field in cost spec/lot spec
#       Using ${date} can be used, basically anywhere in the beancount entry, to insert the
#       date that the scheduler is currently working with. This is useful if you need to name
#       postings with cost specs/lot specs and want to automatically match the date of the
#       transaction. e.g. 0.01 BTC { 7500.00 USD, 2018-05-24 }
#       This could be expressed in the bcspec file like this: 0.01 BTC { 7500.00 USD, ${date} }

import datetime
import shlex
import re
import copy
#import shortuuid
import argparse
import sys

# TODO: swap these out for the actual Beancount Core models
# from beancount_arfsrc.directive import BeancountEntry
# from beancount_arfsrc. import BeancountPosting
# from beancount_arfsrc import BeancountMetadata

# TODO accept these from the command line
bcFilename = "../beancount-data/transactions/next-month.beancount"
schTxsFilename = "../beancount-data/transactions/scheduled-transactions.bcspec"

# TODO accept these from the command line or prompt
startDate = datetime.datetime.strptime("2018-10-01", "%Y-%m-%d")
endDate = datetime.datetime.strptime("2018-10-31", "%Y-%m-%d") 

# Handle command line options
# https://docs.python.org/3/library/argparse.html#module-argparse
argParser = argparse.ArgumentParser(description="The primary Python script that processes the scheduled " +
    "transactions in the bcspec file and provides the appropriate Beancount entries for the data file.")
argParser.add_argument("--save",
    nargs="?",
    type=argparse.FileType('w'),
    default=sys.stdout,
    help="Specify a Beancount data file to save the scheduled transactions to. An optional parameter may " +
         "be specified to indicate what file to use. Otherwise, the default file is used: " +
         bcFilename)
    
args = argParser.parse_args()

#print(args.save)
#exit()

# Holder variable for all of the scheduled transactions
schTxsList = []
# Holder map for tag replacements
tagReplacementMap = {}

with open(schTxsFilename, "r") as schTxsFile:
    currentTxEntry = None
    # setup line reader
    for line in schTxsFile:
        # skip lines which are comments
        if line.startswith(";"):
            continue
        # skip lines which are newlines
        if line.startswith("\n"):
            continue

        # does line start with a date? new transaction!
        if line[0].isdigit():
            # closeout currentTxEntry if we have one
            if currentTxEntry != None:
                # check for the schedule metadata, a scheduled tx must have this; we won't add it otherwise
                if currentTxEntry.hasMetadata('schedule'):
                    schTxsList.append(currentTxEntry)
                currentTxEntry = None
            # start new transction
            lineTokens = shlex.split(line)
            currentTxEntry = BeancountEntry()
            currentTxEntry.Datetime = datetime.datetime.strptime(
                lineTokens[0], "%Y-%m-%d")
            currentTxEntry.Payee = lineTokens[2]
            currentTxEntry.Description = lineTokens[3]
            # process tag replacements if they appear
            # the rest of the tokens on this line will be tags
            for tag in lineTokens[4:]:
                if re.match("\\^\\{.*\\}", tag):
                    print("DEBUG: found a valid tag replacement! " + tag)
                    tagValue = tagReplacementMap.get(tag)
                    if tagValue == None:
                        # Create a new short UUID
                        tagValue = "^" + shortuuid.random()
                    # Now add the tagValue
                    currentTxEntry.addTags(tagValue)
                else:
                    # A "static" tag, which we just pass through
                    currentTxEntry.addTags(tag)
        else:
            # assume metadata or posting, add to current transaction
            # lowercase words indicate metadata
            if re.match("[a-z]+", line.strip()):
                # lowercase words indicate a BeancountMetadata
                lineTokens = line.strip().split(":")
                metadata = beancount_metadata.BeancountMetadata()
                metadata.setKey(lineTokens[0])
                metadata.setValue(lineTokens[1])
                currentTxEntry.addMetadata(metadata)
            else:
                # treat everything else as a BeancountPosting
                # TODO actually parse this and provide more features - this is OK for now
                # because we're just passing through the postings
                currentTxEntry.addRaw(line.strip())

# Now with just the list of scheduled transactions, we can
# run them through to the specified endDate
computedTxsList = []
for schTx in schTxsList:
    schedule = schTx.getMetadataValueFor('schedule')
    # schedule is something like "%m+1" which we need to convert
    # to an appropriate logic block
    if schedule.startswith("%m"):
            # example: "%m+1" for monthly, "%m+12" for annually
            # dealing with months is a bit different than the others because
            # a month doesn't represent a "fixed" duration - the duration is
            # different depending on which month it actually is
        while schTx.Datetime <= endDate:
            monthDelta = int(schedule.strip("%m"))
            year = schTx.Datetime.year
            month = schTx.Datetime.month + monthDelta
            while month > 12:
                year += 1
                month -= 12
            schTx.Datetime = schTx.Datetime.replace(year=year, month=month)
            if schTx.Datetime >= startDate and schTx.Datetime <= endDate:
                computedTxsList.append(copy.deepcopy(schTx))
    elif schedule.startswith("%w"):
        # example: "%w+2" for biweekly
        while schTx.Datetime <= endDate:
            weekDelta = int(schedule.strip("%w"))
            tdelta = datetime.timedelta(weeks=weekDelta)
            schTx.Datetime = schTx.Datetime + tdelta
            if schTx.Datetime >= startDate and schTx.Datetime <= endDate:
                computedTxsList.append(copy.deepcopy(schTx))
    elif schedule.startswith("%d"):
        # example: "%d+7" for weekly
        # TODO finish impl for days
        tdelta = datetime.timedelta(days=7)
        schTx.Datetime = schTx.Datetime + tdelta

# Print out all
for tx in computedTxsList:
    print(tx)

# Print out only the first
# print(schTxsList[0])
#print("... with " + str(len(schTxsList)-1) + " additional entries")

print("\nCopy the above result into the beancount data file!\n")
