#!/usr/bin/env python3

"""Scheduler which uses metadata embedded in the Beancount data file(s).

This is a separate Scheduler version/concept that attempts to provide the
scheduled transactions feature using "embedded metadata" in your existing
beancount file(s).
@author Alex Ford (arf4188@gmail.com)

How does it work?
1. You provide the paths to 1 or more beancount files.
2. You also indicate what the scheduler should generate.
3. Scheduler searches them for the "schedule" metadata entry.
4. Processes them accordingly!
"schedule" metadata reference

| Token | Description |
| ----- | ----------- |
| %d    | Day         |
| %w    | Week        |
| %m    | Month       |
| +N    | Add `N` to the field prefixing this token. |

Examples:
  2018-01-01 * "Landlord" "Rent Payment"
    "schedule": "%m+1"
    Assets:Checking-Account                                         -1000.00 USD
    Expenses:Bills:Rent-and-Mortgage                                 1000.00 USD

    "schedule": "%d+7" is the same as "%w+1"

    "schedule": "%d+2%m+1" would have the transaction increment the month by 1
     along with the day by 2. (Do you really have anything with such a bizzare
     schedule? Yes??? Well now you can model it!)

How do you stop a scheduled directive:
    Use meta property "until" to specify a date-based stopping point.
    Use meta property "occurances" to specify the remaining number of times the directive
    will be scheduled. NOTE: `occurances` is updated by the scheduler, so you
    will actually see the number provided go down for each occurance. If you
    this number arbitrarily, the scheduler will only apply the change from that
    point forward, and only if the change was made to the most recently
    scheduled occurance.
    The scheduler's default behavior is to continue scheduling the directive
    as instructed by the `schedule` meta property indefinitely. Simply remove
    the meta property on the date of the last occurance if at some time in the
    future, the directive should no longer be scheduled.

Type 'pipenv run python scheduler.py --help' for a list of commands.

"""

import logging
logging.basicConfig(level=logging.INFO)
_log = logging.getLogger(__name__)

import argparse

class SchedulerEmbeddedMeta():
    """Implementation of the Scheduler that uses embedded metadata in transactions."""

    def __init__(self):
        pass

    def list(files: list):
        """Parse the beancount file(s) collecting all of the "schedule" metadata
        fields and providing them back as a list of dictionaries:
        [{
            "schedule": "%m+1",
            "date": "2018-01-01",
            "directive": [
                "2018-01-01 * \\"Landlord\\" \\"Apartment Rent Payment\\"",
                "  \\"schedule\\": \\"%m+1\\"",
                "  Assets:Checking-Account                                -1000.00 USD",
                "  Expenses:Bills:Rent-and-Mortgage                        1000.00 USD"
            ],
            "line-number": 41,
            "file": "C:\\path\\to\\file.beancount"
        }]
        """
        _log.debug("Listing for: {}".format(files))
        retValList = []
        for f in files:
            with f:
                # TODO: implement BeancountReader as a class of 'beancount-arfsrc'
                bcReader = beancount_arfsrc.BeancountReader(f)
                directives = bcReader.readDirectives()
                for d in directives:
                    if d.meta["schedule"]:
                        directiveDict = {
                            "schedule": d.meta["schedule"],
                            "date": d.date,
                            "directive": d.raw,
                            "lineNumber": d.lineNumber,
                            "file": d.sourceFile
                        }
                        retValList.append(directiveDict)
        return retValList


def main():
    """Main."""
    pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Scheduler that uses embedded metadata in existing "
            "beancount files to generate future scheduled beancount "
            "transactions."
    )
    # actionArg = parser.add_argument("action",
    #     metavar="action",
    #     help="The Action for the Scheduler to take.",
    #     nargs="?",
    #     type=list,
    #     choices=["help", "generate", "list"]
    # )
    # actionSubParser = parser.add_subparsers(
    #     title="Action Subcommands",
    #     description="Some words about the subcommands.",
    #     help="actionSubParser help goes here",
    # )
    # helpSubParser = actionSubParser.add_parser("action")
    # helpSubParser.add_argument("help", help="Get more help for a given command or topic.")
    # helpSubParser.add_argument("list")
    # helpSubParser.add_argument("generate")
    

    # actionSubParser.add_parser("generate")
    # actionSubParser.add_parser("list")


    parser.add_argument("mainBeancountFile",
        help="Path to the main Beancount file to use.",
        type=argparse.FileType("r", encoding="UTF-8")
    )
    # parser.add_argument("help",
    #     help="Get additional help on a specific argument, command, or topic."
    # )
    # helpSubparser = parser.add_subparsers(help="Get additional help information about subcommands.")
    # parser_a = helpSubparser.add_parser('a', help='a help')
    # parser_a.add_argument("bar", type=int, help="bar help")

    parser.add_argument("-v", "--verbose", action="store_true")
    parser.add_argument("--install-alias",
        help="Installs the 'bean-scheduler' alias into the current user's .bashrc file.",
        action="store_true")

    args = parser.parse_args()
    if args.verbose:
        _log.info("VERBOSE SWITCH DETECTED!")
        _log.setLevel(logging.DEBUG)
    _log.info("Thank you for: {}".format(args.mainBeancountFile))
    list([args.mainBeancountFile])
