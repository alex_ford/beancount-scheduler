"""Beancount Transaction Scheduler that uses one or more Scheduled Transaction 
Files in order to generate transactions.

- Intended to be an improvement over `scheduler_from_file.py`
- Leverages multiple files, assumed to be kept under 
    `beancount-data/transactions/scheduled/active/`
- User should move files out of `active` (into `inactive`, or perhaps some 
    other custom organizational scheme) when no longer applicable
- Users are encouraged to create multiple file(s) and subdirectories to help 
    organize their scheduled transactions.
- This scheduler is fully aware of all transactions under 
    `beancount-data/transactions/`
- The scheduler actively uses transactions that have already been entered to 
    make logical decisions about generated transactions when applicable

# Development Guide

Beancount developer docs:
https://aumayr.github.io/beancount-docs-static/developers/index.html

Beancount API docs:
https://aumayr.github.io/beancount-docs-static/api_reference/index.html

And more general Beancount user docs:
https://aumayr.github.io/beancount-docs-static/users/index.html

"""

import os
import sys
import colorlog
import arrow

from beancount import loader

class SchedulerFromFile2():

    _log = colorlog.getLogger(f"beancount_scheduler.{__name__}")
    _bc_data_dir = "../beancount-data/"
    _bc_main_file = os.path.join(_bc_data_dir, "main.beancount")
    _bc_scheduler_active_dir = os.path.join(_bc_data_dir, "transactions", "scheduled", "active")

    def __init__(self):
        lsh = colorlog.StreamHandler()
        lsh.setFormatter(colorlog.ColoredFormatter())
        self._log.addHandler(lsh)
        self._log.setLevel("DEBUG")
        self._log.debug("New instance of SchedulerFromFile2 has been instantiated.")
        self.validate()

    def validate(self):
        if not os.path.exists(self._bc_data_dir):
            raise RuntimeException(f"Expected {self._bc_data_dir} to exist, but it does not!")
        if not os.path.exists(self._bc_main_file):
            raise RuntimeException(f"Expected {self._bc_main_file} to exist, but it does not!")
    
    def load(self):
        # entries, errors, options = loader.load_file(self._bc_main_file, log_errors=sys.stderr)
        # print(f"{entries[0]}")
        # List the *.beancount files in the 'active' directory
        active_schedules = os.listdir(self._bc_scheduler_active_dir)
        for act in active_schedules:
            act_fullpath = os.path.join(self._bc_scheduler_active_dir, act)
            print(f"{act_fullpath}")
            entries, errors, options = loader.load_file(act_fullpath, log_errors=sys.stderr)
            print(f"{entries}")

def main():
    scheduler = SchedulerFromFile2()
    scheduler.load()
    print("done")

if __name__ == "__main__":
    main()
