#!/usr/bin/env python3

"""Scheduler that uses a 'dumb strategy' to create the next month's file.

    It can only operate on 1 month maximum schedules since it doesn't have awareness of
    additional months beyond the next one. (i.e. you can't schedule transactions 'quarterly',
    'annually', etc.)

    This strategy is to simply:

        1. Parse the previous month's transaction file.
        2. Iterate through it looking for metadata key `schedule`.
        3. Perform the adjustments on the transactions and save to a buffer.
            1. Adjust the date to respect the value of the `schedule` metadata field.
            2. Reset boolean metadata fields.
            3. ...
        4. Save the results to next month's transaction file.

    ## LIMITATIONS

    This implementation has several limitations:

        - It cannot represent scheduled transactions correctly that do not fall
        within a single month. (e.g. quarterly, annual, etc.)

    Author: Alex Richard Ford (arf4188@gmail.com)
    Website: http://www.alexrichardford.com
    License: MIT License
"""

import colorlog
# TODO: replace with 'click'
import argparse
import os
import arrow

from beancount.core import flags
from beancount.core.data import Balance, Transaction
from beancount.parser import parser
from beancount.parser.printer import print_entries

_log = colorlog.getLogger("bean-scheduler")

# TODO: automatically determine current month, and next month files

# TODO: automatically add next month's file to the 'transactions.beancount' index

# BUG: there are issues with this actually handling any schedule other than '%m'

class SchedulerPrevFileDumb():
    """SchedulerPrevFileDumb looks at an existing Beancount data file that
    contains transactions in order to create a new data file."""

    _previous_monthly_file: str = None
    _previous_data = None
    _scheduled_transactions: list = None

    def __init__(self, previous_monthly_file: str):
        """Init a new SchedulerPrevFileDumb object.

        The previous file must exist, and must be a month-based file of Beancount
        transactions.

        The next file does not have to exist, but if it does, a warning will be
        issued as the file will be overwritten when the schedule() function is
        called. The next file does not actually need to be sequential with the
        previous file, but it is often the case that it will be.
        """
        self._previous_monthly_file = previous_monthly_file
        _log.debug(f"_previous_monthly_file = {self._previous_monthly_file}")
        self._previous_data = parser.parse_file(self._previous_monthly_file)
        # _log.debug(f"_previous_data = {self._previous_data}")
        self._scheduled_transactions = []
        # TODO: consider not doing this as part of the __init__ method
        for t in self._previous_data[0]:
            try:
                scheduleMetadata = t.meta["schedule"]
                if scheduleMetadata != None:
                    date: arrow = arrow.get(t.date)
                    payee: str = t.payee
                    narr: str = t.narration
                    _log.info(f"Automatically scheduling transaction to \"{payee}\" for \"{narr}\".")

                    # Now we need to perform modifications to the transaction
                    # to meet the scheduling rules.
                    if "initiated" in t.meta:
                        t.meta["initiated"] = ""
                    if "added-to-settle-up" in t.meta:
                        t.meta["added-to-settle-up"] = False
                    if "estimated" in t.meta:
                        t.meta["estimated"] = True

                    # Metadata 'schedule' can be:
                    #    %m+N
                    #    %w+N
                    #    %d+N
                    mod = scheduleMetadata[0:2]
                    amt = int(scheduleMetadata[2:4])
                    _log.debug(f"mod = {mod}, amt = {amt}")
                    newDate = None
                    # TODO: the scheduler can't handle months beyond +1...
                    if mod == "%m":
                        newDate = date.shift(months=+amt)
                    elif mod == "%w":
                        newDate = date.shift(weeks=+amt)
                    elif mod == "%d":
                        newDate = date.shift(days=+amt)
                    elif mod == "%y":
                        _log.warning("Encountered schedule metadata with currently unhandled value. Skipping!")
                        continue
                    else:
                        raise RuntimeError(f"Unknown schedule specifier: '{mod}'")
                    newTx = Transaction(t.meta, newDate.format("YYYY-MM-DD"), "!", t.payee, t.narration, t.tags, t.links, t.postings)
                    self._scheduled_transactions.append(newTx)
            except KeyError:
                """This will happen when/if a directive doesn't have the
                'schedule' metadata, which is fine and expected for loads of
                directives."""
                continue
        _log.info(f"Found {len(self._scheduled_transactions)} transactions for scheduling!")


    def write_scheduled_transactions(self, out_file_path: str, merge: bool = False):
        """Writes the next monthly file as determined by this Scheduler. If 'merge' is set to True, then
        the scheduled transactions will be appended to the file instead of overwritting it."""
        _log.debug(f"out_file_path = {out_file_path}")
        out_file_path_dirname = os.path.dirname(out_file_path)
        _log.debug(f"out_file_path_dirname = {out_file_path_dirname}")
        os.makedirs(out_file_path_dirname, exist_ok=True)
        if not merge:
            with open(out_file_path, "w") as out_file:
                print_entries(self._scheduled_transactions, file=out_file)
        else:
            with open(out_file_path, "a") as out_file:
                print_entries(self._scheduled_transactions, file=out_file)  


def main():
    """Main method for direct use from the CLI."""
    logHandler = colorlog.StreamHandler()
    logHandler.setFormatter(colorlog.ColoredFormatter())
    _log.addHandler(logHandler)
    _log.setLevel("INFO")

    _log.info("Beancount Scheduler / scheduler_prev_file_dumb.py")

    arg_parser = argparse.ArgumentParser(
        description="A 'dumb' scheduler which uses a specified previous "
                    "monthly Beancount transaction file in order to generate "
                    "a new monthly Beancount transaction file."
    )
    arg_parser.add_argument("previousTxFile",
        type=str
    )
    arg_parser.add_argument("nextTxFile",
        type=str
    )
    arg_parser.add_argument("-f", "--force",
        help="Forces the scheduler to write to output file even if it already exists.",
        action="store_true")
    arg_parser.add_argument("-m", "--merge",
        help="Causes the scheduler to merge the output to file even if it already exists.",
        action="store_true")
    arg_parser.add_argument("--debug", "--verbose",
        help="Enables debug logging to the console output.",
        action="store_true")

    args = arg_parser.parse_args()
    if args.debug:
        _log.setLevel("DEBUG")
        _log.debug("DEBUG LOGGING IS ENABLED")

    _log.debug(f"args = {args}")

    if os.path.exists(args.previousTxFile) is False:
        _log.error(f"Specified file for previous transactions does not exist! ({args.previousTxFile})")
        exit(1)
    
    if os.path.exists(args.nextTxFile) and (args.force is False and args.merge is False):
        _log.error(f"Specified file for next transactions already exists! ({args.nextTxFile})")
        _log.error(f"Refusing to clobber file unless --force switch is specified, or merge if --merge switch is specified.")
        exit(1)

    sched = SchedulerPrevFileDumb(args.previousTxFile)
    sched.write_scheduled_transactions(args.nextTxFile, args.merge)
    _log.info(f"New transactions have been saved to: {args.nextTxFile}")
    _log.info("Scheduler has finished!")

if __name__ == "__main__":
    main()
