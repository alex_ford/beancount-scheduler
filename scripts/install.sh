#!/bin/bash

###
###
###
### Author: Alex Ford <arf4188@gmail.com>

THIS_SCRIPT="$(realpath ${0})"
SCRIPT_DIR="$(dirname ${THIS_SCRIPT})"
PROJECT_DIR="$(dirname ${SCRIPT_DIR})"

CURRENT_DIR="$(pwd)"

cd "${PROJECT_DIR}"

# Simple print/logging functions
function error() {
    EXIT_CODE="$1"
    MSG="$2"
    echo -e "[ \e[31mERRO\e[0m ] ${MSG}"
    exit ${EXIT_CODE}
}
function warn() {
    MSG="$1"
    echo -e "[ \e[33mWARN\e[0m ] ${MSG}"
}
function info() {
    MSG="$1"
    echo -e "[ \e[30mINFO\e[0m ] ${MSG}"
}
function debug() {
    MSG="$1"
    echo -e "[ \e[32mDBUG\e[0m ] ${MSG}"
}

# Ensure the virtual environment is installed
pipenv install

# Now make the sym link on the user's path as `bean-sched`
# Resolve sudo, if we should use it or not
which sudo >/dev/null 2>&1
if [ $? == 0 ]; then
    USE_SUDO="sudo"
else
    USE_SUDO=""
fi
command -v termux-info
if [ $? == 0 ]; then
    echo "[ INFO ] Android/Termux!"
    USE_SUDO=""
fi

# Resolve Python 3 command(s)
# Try 'python3' first, then on fail, try 'python' and verify that it is v3.
PYTHON3="python3"
apt list "${PYTHON3}" | grep "${PYTHON3}"
if [ $? != 0 ]; then
    PYTHON3="python"
    python --version | grep "Python 3"
fi
if [ $? != 0 ]; then
    error 1 "could not resolve Python 3 commands for this system!"
fi
debug "Python 3 command resolved to: ${PYTHON3}"

which "${PYTHON3}" >/dev/null 2>&1
if [ $? != 0 ]; then
    warn "Python 3 was not installed on this system! Will install it now..."
    "${USE_SUDO}" apt install "${PYTHON3}"
else
    info "Python 3 already installed!"
fi

PIP="pip"
which "${PIP}" >/dev/null 2>&1
if [ $? != 0 ]; then
    warn "Pip was not installed on this system! Will install it now..."
    "${USE_SUDO}" apt install python3-pip
else
    info "Pip already installed!"
fi

PIPENV="pipenv"
which "${PIPENV}" >/dev/null 2>&1
if [ $? != 0 ]; then
    warn "Pipenv was not installed on this system! Will install it now..."
    "${USE_SUDO}" "${PYTHON3}" -m pip install pipenv
else
    info "Pipenv already installed!"
fi

# Save current directory so we can go back to it later
# This is preferred over pushd/popd
ORIGIN_DIR="$(pwd)"
cd "${PROJECT_HOME_DIR}"
if [ $? != 0 ]; then
    error 1 "PROJECT_HOME_DIR (${PROJECT_NAME_DIR}) could not be accessed!"
fi
#${PYTHON3} -m pipenv --venv >/dev/null 2>&1
#if [ $? != 0 ]; then
"${PYTHON3}" -m pipenv install
#else
#    info "existing virtual environment $(pipenv --venv) detected!"
#fi
cd "${ORIGIN_DIR}"

# Attempt to resolve the "user local" bin directory
BIN_DIR="/usr/local/bin"
if [ ! -e "${BIN_DIR}" ]; then
    info "${BIN_DIR} doesn't exist on this system, trying next option..."
    BIN_DIR="$(dirname $(which bash))"
    #error 1 "user's local bin directory could not be determined!"
    #exit 1
fi
info "installing bin links to: ${BIN_DIR}"

# Usage: make_bin_link name
# This expects a ./project/scripts/run_bash/run-name.sh to exist.
function make_bin_link() {
    NAME="${1}"
    RUN_SCRIPT_PATH="${2}"
    BIN_LINK_PATH="${BIN_DIR}/${NAME}"
    info "making bin link at: ${BIN_LINK_PATH}"
    if [ -h "${BIN_LINK_PATH}" ]; then
        warn "existing bin link found -- unlinking it now!"
        ${USE_SUDO} unlink "${BIN_LINK_PATH}"
    fi
    ${USE_SUDO} ln -sv "${RUN_SCRIPT_PATH}" "${BIN_LINK_PATH}"
    if [ $? != 0 ]; then
        error 1 "encountered a problem while creating bin links!"
    fi
}

# Create bin links
make_bin_link "bean-sched" "$(realpath ${SCRIPT_DIR}/run.sh)"

cd "${CURRENT_DIR}"
