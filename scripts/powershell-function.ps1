<#
    USAGE:
    bean-scheduler $(Resolve-Path .\transactions\2019\201905.beancount) "$(Resolve-Path .\transactions\2019)\201906.beancount"
#>
function bean-scheduler() {
    $BEANCOUNT_SCHEDULER_DIR = Resolve-Path "${HOME}/Workspaces-Personal/beancount/beancount-scheduler"
    $BEANCOUNT_DATA_DIR = Resolve-Path "${HOME}/Workspaces-Personal/beancount/beancount-data"

    # TODO: if we change the working directory, then we need to ensure any paths passed in via args are first made into absoluete paths

    $CWD = Get-Location
    
    Set-Location ${BEANCOUNT_SCHEDULER_DIR}
    
    pipenv run bean-scheduler $args
    
    Set-Location $CWD
}
