#!/bin/bash

### Run script for bean-sched (Beancount Scheduler) for Bash. This should
### end up being symbolically linked to a location on the user's path after
### running the install.sh script. Usage is simple, with no arguments, this
### attempts to perform the most default and typical action of taking the
### current Beancount transaction file, and producing the next transaction file.
### If arguments are given, then it is up to the user to fully instruct
### bean-sched on what to do.
###
### Author: Alex Ford <arf4188@gmail.com>

THIS_SCRIPT="$(realpath ${0})"
SCRIPT_DIR="$(dirname ${THIS_SCRIPT})"
PROJECT_DIR="$(dirname ${SCRIPT_DIR})"

CURRENT_DIR="$(pwd)"

cd "${PROJECT_DIR}"

if [ $# -eq 0 ]; then
    ## default behavior with no args to bean-sched
    CURRENT_BEANCOUNT_FILE="$(bean-determine current)"
    NEXT_BEANCOUNT_FILE="$(bean-determine next)"
    pipenv run main --merge "${CURRENT_BEANCOUNT_FILE}" "${NEXT_BEANCOUNT_FILE}"

    ## Auto-open the file containing the new scheduled transactions in VS Code if
    ## the user ran this script from within VS Code!
    if [ "${TERM_PROGRAM}" == "vscode" ]; then
        echo "[ INFO ] detected running from VS Code! Opening scheduled file in editor now..."
        code "${NEXT_BEANCOUNT_FILE}"
    fi
else
    pipenv run main $@
fi

cd "${CURRENT_DIR}"

exit 0 # below this line isn't currently production ready code!

## Determine key variables values
# TODO: the following are here to support the next TODO! Do not remove!

# main path to the root of the user's Beancount Data repository/directory
BEANCOUNT_DATA_DIR="${HOME}/Data/Personal/beancount-data"

# this first one is for the yearly index file
#    e.g. transactions/transactions.beancount
TX_YEARLY_INDEX_FILE="${BEANCOUNT_DATA_DIR}/transactions/transactions.beancount"

# this next one is for the monthly index file
#    e.g. transactions/{YYYY}/{YYYY}.beancount
YEAR="$(date +%Y)"
echo "[ DBUG ] YEAR => ${YEAR}"
TX_MONTHLY_INDEX_DIR="${BEANCOUNT_DATA_DIR}/transactions/${YEAR}/"
TX_MONTHLY_INDEX_FILE="${BEANCOUNT_DATA_DIR}/transactions/${YEAR}/${YEAR}.beancount"

if [ ! -e "${TX_MONTHLY_INDEX_DIR}" ]; then
    mkdir -v "${TX_MONTHLY_INDEX_DIR}"
fi

# TODO: automatically update Transactions Index File(s) with new monthly and/or yearly files
