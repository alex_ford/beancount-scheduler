#!/usr/bin/env python3

"""Install script for Beancount Scheduler.

This is intended to work on both Linux and Windows systems! (Eventually!)

Author: Alex Richard Ford (arf4188@gmail.com)
Website: http://www.alexrichardford.com
License: MIT License
"""

import logging
logging.basicConfig(level=logging.INFO)
_log = logging.getLogger(__name__)

import os

USER_HOME=os.path.expanduser("~")
USER_BASHRC_FILE="{}/.bashrc".format(USER_HOME)
_log.info("USER_BASHRC_FILE is: {}".format(USER_BASHRC_FILE))

try:
    if "alias bean-scheduler" in open(USER_BASHRC_FILE).read():
        _log.error("Your '.bashrc' file already contains an alias for 'bean-scheduler'!")
        _log.error("Refusing to clobber/clutter your '.bashrc' file!")
        exit(1)
    with open(USER_BASHRC_FILE, "a") as f:
        f.write("alias bean-scheduler='pushd ~/Workspaces-Personal/beancount/beancount-scheduler/ && pipenv run python beancount-scheduler/scheduler-embedded-meta.py && popd'")
except Exception as e:
    _log.error(e)
_log.info("All done! 'bean-scheduler' should now be ready for use!")
