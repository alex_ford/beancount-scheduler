"""Scheduler for Beancount directives.

This mini-project is concerned with the scheduling of beancount directives,
which includes transactions, events, etc.
"""

from setuptools import find_packages, setup

setup(
    name='beancount-scheduler',
    version='0.1.0',
    author='Alex Richard Ford',
    author_email='arf4188@gmail.com',
    packages=find_packages(exclude="tests, docs"),
    scripts=['scripts/install-scheduler-alias.py'],
    url='http://www.alexrichardford.com',
    license='LICENSE',
    description='A small accessory project to the Beancount CLI personal finance app which provides scheduling features.',
    long_description=open('README.txt').read(),
    install_requires=[
    ],
)
