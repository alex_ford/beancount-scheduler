# Beancount Scheduler (`beancount-scheduler`)

This project builds off of the Beancount double-entry accounting CLI project. <http://furius.ca/beancount/>

**Beancount Scheduler** is all about scheduling transactions and contains code, scripts, etc. to help with that in the Beancount ecosystem. As is becoming a common convention, you should create your own "beancount-data" directory, one step up from the directory where this README.md/code resides. In this directory, you should copy the provided `scheduled-transactions.bcspec-sample` file to `..\beancount-data\scheduled-transactions.bcspec`. This will allow the default parameters for most functions to "just work".

Scheduler aims to be a powerful extension of your Beancount workflow! Here's a list of the features (some still WIP):

-   [x] Define mechanism for specifying schedule of a specific transaction
-   [x] Use of a spec file to record scheduled transactions separate from the data file
-   [x] Allow addition operation on day, week, and year term of the date
-   [x] Create scheduled transactions within a specific range
-   [ ] Use either a spec file, or simply use previous transactions in your data file
-   [ ] Automatic amount calculation based on prior transactions
    -   Options
        -   Use Last Amount - uses the last known amount for this transaction.
        -   Average Last 3 Amounts - uses a moving average of the last 3 amounts.
        -   Annual Amounts - uses the last amount for this transaction that was scheduled one year ago (useful to estimate things like electricity/heating)
-   [ ] Auto-creation of escrow/release transactions
    -   Escrow/release transactions are a pair of transactions which address the problem of different posting days when money is moved between accounts. This uses a separate Escrow account to track the funds as they are being moved.
    -   This implements the metadata field 'useEscrow' (TRUE/FALSE)
    -   As well as the metadata field 'release-date' which can be a timedelta specifier (e.g. `%d+4`)
-   [ ] Auto-linking of related transactions
    -   Escrow/release transactions are automatically linked with a short UUID
    -   Other links can be established using the `^{link-name}` specifier
    -   Links will be generated using a short UUID scheme when the scheduler runs

**Project Maturity**: Alpha (2/4)

The project is reaching improved maturity, and can be useful for "power users" or those who have specific domain knowledge which can help get past the lack of polish. The software code can therefore be useful, but likely not to the general public. Beyond lack of polish, it's also expected that a fair number of bugs still exist.

**Author**: Alex Richard Ford (arf4188@gmail.com)

**Website**: <http://www.alexrichardford.com>

**License**: [MIT License](./LICENSE)

# Dependencies

## Source Code/Repositories

In addition to this Git repo, you will also need the following other repos:

-   beancount-arfsrc

## Libraries (PyPi)

The following Python libraries are required:

* [Beancount]()
* [Arrow](https://arrow.readthedocs.io/en/latest/)
* [shortuuid](https://github.com/skorokithakis/shortuuid)
* colorlog

`pipenv` is the recommended way to handle installing them. See below for more details.

# Quick Start (for End Users)

## Install Python libraries

```bash
pipenv install
```

## Run from source directory

```bash
pipenv run bean-scheduler
```

## Install profile function/alias

```bash
pipenv run install-bean-scheduler
```

## Create user data directory

1.  If you havne't done so already, create your `..\beancount-data\` directory now.

!!! NEEDS UPDATE !!!

2.  Copy the `scheduled-transactions.bcspec-sample` to `..\beancount-data\scheduled-transactions.bcspec`
3.  Open up the `..\beancount-data\scheduled-transactions.bcspec` file and insert your desired transactions (as standard Beancount transactions) into it.
4.  Provide the 'schedule' metadata key-value along with the desired frequency specification. (See Frequency Reference section of this README for more details) Here's an example transaction that will be scheduled once every month:
    ```beancount
    2018-04-28 ! "Boyfriend" "Portion of various bills and IOUs"
      schedule: "%m+1"
      Income:Reimbursements:Family                -85.30 USD ; cell phone
      Income:Reimbursements:Family                -23.25 USD ; gym
      Assets:Primary-Checking
    ```
5.  Execute `python .\run-scheduler.py > tmp.beancount` and review the output!
6.  Copy the results into your Beancount data file if you're happy with it.

## Run Beancount Scheduler

```bash
pipenv run beancount-scheduler --help
```


# Frequency Specification Reference

The scheduler works by looking at a Beancount data file for transactions that contain the `schedule` metadata field. Here's an example:

```beancount
2019-04-19 * "Payee"  "Narration"
  schedule: "%m+1"
  Assets:Checking                   -250.00 USD
  Liabilities:Credit-Card            250.00 USD
```

The `schedule` metadata field takes in a frequency specification loosely based on the Python timedelta implementation.

| Expression | Description                             | Example Usage      |
| ---------- | --------------------------------------- | ------------------ |
| `%w`       | Add to the week component of the date.  | `schedule: "%w+1"` |
| `%d`       | Add to the day component of the date.   | `schedule: "%d+4"` |
| `%m`       | Add to the month component of the date. | `schedule: "%m+2"` |

For example, the following transaction will be scheduled 2 weeks after the current transaction date.

```beancount
2018-05-03 ! "My Job"   "💰Bi-weekly Paycheck"
  schedule: "%w+2"
  Assets:US:Banking:Checking                 1000.00 USD
  Income:Paycheck
```

# How the Scheduler determines next occurances

Coming soon...

# Common Conventions

In all of my beancount-\* repos, I follow the following conventions:

* Any file containing primarily Beancount Directives should use the extension `.beancount`
* Beancount Directives, specifically Transactions, are sorted into *monthly* files.
* User's Beancount data is stored in a directory located one level above this one. (i.e. `../beancount-data/`)
* That data folder uses the name `beancount-data`

# Development Quick Start

This section of the README is for those looking to work on the source code for this project.

1. Run `pipenv install --dev`
2. Run `code .`

## Testing

```bash
pipenv run test
```
