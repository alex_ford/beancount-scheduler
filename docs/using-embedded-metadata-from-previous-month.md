Using Embedded Metadata from Previous Monthly Transaction File
==============================================================

**Assumptions**

* All scheduler input data is stored as metadata in transactions.
* Transactions are organized into files separated by month.
* Scheduler consumes exactly one monthly transaction file as input.
* Scheduler generates exactly one monthly transaction file as output.
* User may specify any monthly file as input, but Scheduler will only produce the next month in chronological sequence.
    * e.g. if given 201901.beancount as input, will generate 201902.beancount as output
    * e.g. if given 201912.beancount as input, will generate 202001.beancount as output
* Beancount data uses a 'main.beancount' file to declare imports for monthly transaction files.

# Shortcomings

* Difficult to schedule transactions with schedules greater than one month. (e.g. `%m+2`, `%w+12`)
    * Possible solution 1
        * Could use "placeholder" transactions to carry over scheduler data which can make it possible to solve this problem.
        * Placeholders can either be "real" or "transparent"
            * Real placeholders are simply transactions with zero'd amounts so they don't effect any balances.
            * Transparent placeholders are commented-out transactions, which don't show in certain interfaces (like Fava), and also don't effect any balances.
                * Transparent placeholders might be better simply because they can be discarded by tools that don't need them. (They would be treated simply as comments and ignored by the core Beancount tools, for example.)
* Difficult to schedule transactions with schedules less than one month. (e.g. `%w+2`, `%d+4`)
    * In the simple monthly scheduler handling, you could sort-of work around this by having two transactions with `%m+1`, but this will not always work as some months will need three instances of a bi-weekly transaction.
    * Possible solution 1
        * ...
