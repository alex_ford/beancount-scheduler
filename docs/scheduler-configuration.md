# Scheduler Configuration

**DRAFT ... DRAFT ... DRAFT ... DRAFT ... DRAFT .. DRAFT**

This document outlines what needs to be done in your Beancount Data repository in order to make this application work.

## How it Works

The Scheduler is run either on-demand, or by integration with other scripts/workflows. When ran, it looks for _active_ Beancount files that contain _Scheduler Config Metadata_, processes them, updates the metadata, and writes the generated Beancount Directives into the correct Beancount Data files.

## Structure

At the moment, this only supports the following directory structure:

```filesystem
./beancount-data/
./beancount-data/transactions/
./beancount-data/transactions/{{ YEAR }}/
./beancount-data/transactions/{{ YEAR }}/{{ YEAR }}{{ MONTH }}.beancount
./beancount-data/transactions/scheduled/
./beancount-data/transactions/scheduled/active/
./beancount-data/transactions/scheduled/active/my-paycheck.beancount
./beancount-data/transactions/scheduled/active/foobar-world.beancount
./beancount-data/transactions/scheduled/inactive/
./beancount-data/transactions/scheduled/inactive/old-paycheck.beancount
```

## Overview of a Scheduled Beancount File

Here is an example `.beancount` file that is read by the scheduler:

```beancount
schedule: "%y+1"
start-date: "2019-05-31"
end-date: "2020-05-31"
next-scheduled-date: ""
----
{{ DATE }} ! "TRANSFER"   "For annual Life Insurance Premium"
  Assets:Savings:Life-Insurance-Reserve  -150.00 USD
  Assets:Checking

{{ DATE }} ! "Insurance Company"   "Annual Life Insurance Premium"
  Assets:Checking                        -150.00 USD
  Expenses:Other-Insurance:Life-Insurance
```

This file would live here:

```filesystem
./beancount-data/transactions/scheduled/active/life-insurance-payment.beancount
```

When creating or modifying this scheduled file, the first part of the file consists of the scheduler metadata, separated by a dashed line (`----`), with the remaining contents being a form of a [Beancount Template](../../../../beancount-templates/README.md).

## Special Handling of the Beancount Template

The Beancount Template project defines several expressions which are handled in specific ways which are changed slightly when the Scheduler processes them. "Template Processor" will be used to refer to the Beancount Template Processor's default handling in the list of differences noted below.

-   The `{{ DATE }}` expression will be set to the next scheduled date, instead of the current date.
-   ...

## Scheduled Result

Using the example scheduled `.beancount` file above, the Scheduler would end up producing the following:

```beancount
2019-05-31 ! "TRANSFER"   "For annual Life Insurance Premium"
  Assets:Savings:Life-Insurance-Reserve  -150.00 USD
  Assets:Checking

2019-05-31 ! "Insurance Company"   "Annual Life Insurance Premium"
  Assets:Checking                        -150.00 USD
  Expenses:Other-Insurance:Life-Insurance
```

## Scheduler Config Metadata

### Summary Reference

-   `schedule` (REQUIRED)
    -   The schedule expression.
    -   `%w+2`
-   `start-date` (REQUIRED)
    -   Date when the Scheduler should start scheduling.
    -   "2019-05-31"
-   `end-date`
    -   Date when the Scheduler should stop scheduling.
    -   If not specified (not present, or empty date), the Scheduler will simply continue generating this file with no set end.
    -   Mutually exclusive with: `next-occurrence`, `max-occurrence`
    -   ""
-   `next-scheduled-date`
    -   Date when the Scheduler will generate the next instance.
    -   The user doesn't need to provide this, as the Scheduler will create/update this property at runtime. If not specified, the `start-date` is used.
    -   "2019-05-31"
-   `next-occurrence`
    -   Occurrence number the Scheduler will use for the next instance.
    -   Mutually exclusive with: `end-date`
    -   0
-   `max-occurrence`
    -   The maximum number of occurrences the Scheduler will generate.
    -   Mutually exclusive with: `end-date`
    -   0

## Schedule Expression

This is represented by the `schedule` config metadata property and controls when the scheduler attempts to schedule the Beancount file in question.

`%w+2` simply means "bi-weekly".
`%y+1` simply means "annually".

## Time Bounding the Scheduler

By default, the Scheduler will generate directives for all dates in the past, up to 30 days from now. Any scheduled file that falls outside of this will be ignored.

This behavior can be tweaked... COMING SOON...
