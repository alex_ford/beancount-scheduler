import logging
import sys
sys.path.append("../")
from beancount_scheduler import SchedulerPrevFileDumb

logging.basicConfig(level=logging.DEBUG)
_log = logging.getLogger(__name__)

def test_scheduler_prev_file_dumb():
    scheduler = SchedulerPrevFileDumb("../../beancount-data/transactions/2019/201901.beancount", "../../beancount-data/transactions/2019/201902.beancount")
    scheduler.write_scheduled_transactions()
